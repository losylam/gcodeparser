gcodeparser
==

Utilitaire python de post traitement pour ajouter des attaches à des fichiers gcode

Ne prend en compte que les lignes droites.

Paramètres :
* nombre d'attache
* hauteur et épaisseur des attaches