#!/env/python
# coding : utf-8

"""Usage:
  gcodeparser.py [--length <mm> --height <mm> --number <N>] INPUT OUTPUT 
  gcodeparser.py --test

A Python script to add attachemnent for CNC cutting

Arguments: 
  INPUT                input file (.gcode)
  OUTPUT               output file (.gcode)

Options:
  -h --help            Display help 
  --height <mm>        Height of the attachment [default: 1]
  -l, --length <mm>    Length of the attachement [default: 4]
  -n, --number <N>     Number of attachment [default: 3]
 """


import numpy as np
from docopt import docopt

GCODE = {
    'G':{'type':'move'},
    'M':{'type':'set'},
    
}

GCODES_CODES = 'GMFSXYZ'
GCODES_INT = 'GMT'

def points_from_seg(pt1, pt2, e):
    a = pt1['pos']
    b = pt2['pos']
    if np.linalg.norm(b - a) > e:
        l = (pt2['dist'] - pt1['dist'] - e)/2 # dist from a and b
        ab = b - a                            
        ab_norm = ab/np.linalg.norm(ab)
        
        c = a + l*ab_norm       # pt close to a
        d = b - l*ab_norm       # pt close to b
        return c, d
    return a, b

def calculate_length(g):
    l = 0
    for i in range(len(g)-1):
        l += np.linalg.norm(g[i+1]['pos'] - g[i]['pos'])
    return l

class GcodeParser:
    def __init__(self, 
                 gcode_file,
                 output = None,
                 hop_height = 1,
                 hop_length = 4,
                 hop_number = 3):

        self.pos = np.zeros(2)
        self.z = 0        
        self.action = 0
        self.f = 0

        self.gcode_filename = gcode_file
        with open(gcode_file, 'r') as f:
            lines = f.readlines()
        
        for i in range(len(lines)):
            lines[i] = lines[i].strip()

        self.gcode_lines = lines
        self.gcode = []

        self.hop_length = hop_length
        self.hop_height = hop_height
        self.hop_number = hop_number

        self.hop = {}
        if output:
            self.parse_gcode()
            self.analyse_gcode()
            self.find_path()
            self.get_dist()
            self.find_hop_segs()
            self.create_hops()
            self.reexport_gcode(output)

    def parse_gcode(self):
        for l in self.gcode_lines:
            self.gcode.append(self.parse_line(l))

    def parse_line(self, l):
        if len(l) == 0:
            return {'type':'blank', 'raw':l}
        elif l[0] in ';(%':
            return {'type':'comment', 'raw':l}
        else:
            try:
                return {'type':'gcode', 'gcode':self.parse_gcode_line(l), 'raw':l}
            except:
                return {'type':'error', 'raw':l}

    def parse_gcode_line(self, l):
        dic_g = {}
        next_g = l[0]
        val = ''
        for i in l[1:]:
            if i in GCODES_CODES:
                dic_g[next_g] = float(val.strip())
                next_g = i
                val = ''
            else:
                val += i
        dic_g[next_g] = float(val.strip())

        for i, it in dic_g.items():
            if i in GCODES_INT:
                it = int(it)
        return dic_g

    def analyse_gcode(self):
        for i in self.gcode:
            if 'gcode' in i:
                gg = i['gcode']
                if 'G' in gg:
                    if gg['G'] == 0:
                        i['action'] = 'move'
                    elif gg['G'] == 1:
                        i['action'] = 'machine'


    def find_path(self):
        self.path_bottom = []
        self.path = []
        lst_z = []
        new_path = []
        pos = np.zeros(2)
        z = 0
        new_point = {'id':None,
                     'pos':self.pos.copy(),
                     'Z':self.z,
                     'G':self.action,
                     'F':self.f}
        
        for i, it in enumerate(self.gcode):
            if 'action' in it:#it['action'] == 'machine' or it['action'] == 'move':
                z_change, g_change = self.actualise(it['gcode'])
                last_point = new_point.copy()
                new_point = {'id':i,
                             'pos':self.pos.copy(),
                             'Z':self.z,
                             'G':self.action,
                             'F':self.f}

                if self.action == 1:
                    if z_change or g_change:
                        if len(new_path)>0:
                            new_path = new_path[:]
                            z = round(np.mean([i['Z'] for i in new_path if i['Z'] < 0]), 2)
                            self.path.append({'path':new_path[:],
                                              'length':calculate_length(new_path),
                                              'z':z})
                            if z not in lst_z:
                                lst_z.append(z)
                        new_path = [last_point, new_point]
                    else:
                        new_path.append(new_point)
            

        lst_z.sort()
        self.lst_z = lst_z
        self.min_z = min(lst_z)
        new_path = new_path[:]
        self.path.append({'path':new_path[:],
                          'length':calculate_length(new_path),
                          'z':np.mean([i['Z'] for i in new_path if i['Z'] < 0])})
        
        for p in self.path:
            if p['z'] < 2*self.min_z/3:
                self.path_bottom.append(p)


    def actualise(self, dic_g):
        g_change = False
        z_change = False
        self.last_pos = self.pos.copy()
        if 'X' in dic_g:
            self.pos[0] = dic_g['X']
        if 'Y' in dic_g:
            self.pos[1] = dic_g['Y']

        if 'Z' in dic_g:
            if dic_g['Z'] != self.z:
                self.z = dic_g['Z']
                z_change = True
    
        if 'F' in dic_g:
            self.f = dic_g['F']

        if dic_g['G'] != self.action:
            g_change = True
            self.action = dic_g['G'] 

        return z_change, g_change

    def get_dist(self):
        for i in self.path:
            p = i['path']
            p[0]['dist'] = 0.0
            for i_pt in range(1, len(p)):
                p[i_pt-1]['seg_length'] = np.linalg.norm(p[i_pt]['pos'] - p[i_pt-1]['pos'])
                p[i_pt]['dist'] = p[i_pt-1]['dist'] + p[i_pt-1]['seg_length']
                
            p[-1]['seg_length'] = np.linalg.norm(p[0]['pos'] - p[-1]['pos'])

    def find_hop_seg(self, path):
        l = path['length']
        lst_pts = path['path']
        path['hop_seg'] = []
        
        n_seg = 10               # number of segment to find the most longer one
        
        cur_step = 0

        for i_p, p in enumerate(lst_pts[1:], 1):
            if p['dist'] >= cur_step*l/self.hop_number:
                max_length = p['seg_length']
                hop_a = i_p
                for j in range(1,n_seg):
                    if i_p + j < len(lst_pts) and lst_pts[i_p + j]['seg_length'] > max_length:
                        max_length = lst_pts[i_p + j]['seg_length']
                        hop_a = i_p + j
                path['hop_seg'].append(hop_a)
                cur_step += 1
            if cur_step >= self.hop_number:
                break

    def find_hop_segs(self):
        for i in self.path_bottom:
            if i['length'] > self.hop_length * 6:
                self.find_hop_seg(i)
            else:
                i['hop_seg'] = []
            
    def create_hop(self, path):
        path['hop'] = []
        for h in path['hop_seg']:
            if h < len(path['path'])-1:
                c,d = points_from_seg(path['path'][h], path['path'][h+1], self.hop_length)
            else:
                c, d =points_from_seg(path['path'][h], path['path'][0], self.hop_length)
            id_start = path['path'][h]['id']
            f_hop = path['path'][h]['F']
            dic_hop = {'c': c,
                       'd': d,
                       'id_start': id_start,
                       'z': path['z'],
                       'f':f_hop}

            self.hop[id_start] = dic_hop 

    def create_hops(self):
        for p in self.path_bottom:
            self.create_hop(p)

    def reexport_gcode(self, filename):
        txt = ''
        for i, it in enumerate(self.gcode):
            if len(it['raw']) == 0:
                txt += '\n'
            elif it['raw'][0] != '(':
                txt += it['raw'].replace('(Penetrate)', '')+'\n'
            if i in self.hop:
                txt += '\n'
                txt += 'G01 X%s Y%s F%s\n'%(self.hop[i]['c'][0], self.hop[i]['c'][1], self.hop[i]['f'])
                txt += 'G01 Z%s F200\n' % (self.hop[i]['z'] + self.hop_height)
                txt += 'G01 X%s Y%s F%s\n'%(self.hop[i]['d'][0], self.hop[i]['d'][1], self.hop[i]['f'])
                txt += 'G01 Z%s F200\n' % self.hop[i]['z'] 
                txt += 'G01 F1000\n'
                txt += '\n'

        with open(filename, 'w') as f:
            f.write(txt)
        

if __name__ == '__main__':
    arguments = docopt(__doc__)

    print arguments
    if arguments['--test']:
        g = GcodeParser('exemple_simple.gcode')
        g = GcodeParser('decoupe_0004.gcode')

        g.parse_gcode()
        g.analyse_gcode()
        g.find_path()
        g.get_dist()
        g.find_hop_segs()
        g.create_hops()

    else:
        g = GcodeParser(arguments['INPUT'],
                        output = arguments['OUTPUT'],
                        hop_height = float(arguments['--height']),
                        hop_length = float(arguments['--length']),
                        hop_number = int(arguments['--number']))
                        

